#!/bin/bash

set -e

if [ "$SSH_ROOT_PASS" ]; then
  echo "Using Root Password from Environment"
  echo "root:$SSH_ROOT_PASS" | chpasswd
  sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config
  sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
fi;

case "$1" in
    sshd)
        exec /usr/sbin/sshd -D
        ;;
    *)
        exec "$@"
esac
exit 1
