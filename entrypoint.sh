#!/usr/bin/env bash

[ "$DEBUG" = "true" ] && set -x

case "$1" in
    cron)
      /usr/bin/crontab /etc/crontab
      /usr/sbin/cron -f
      ;;
    sync)
      /ska/scripts/syncd.php --systemd --user keys-sync
      ;;
    httpd)
      /usr/sbin/apache2ctl -DFOREGROUND
      ;;
    *)
      exec "$@"
esac
exit 1