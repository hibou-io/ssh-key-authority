FROM php:7.2-fpm
MAINTAINER Hibou Corp <hello@hibou.io>

RUN useradd -m -d /ska -s /bin/sh -u 166 -g 33 keys-sync \
    && apt-get update \
    && apt-get install -y \
        apache2 \
        libldap2-dev \
        cron \
        rsync \
        openssh-client \
    && /usr/sbin/a2enmod ldap authnz_ldap proxy proxy_fcgi \
    && docker-php-ext-install mysqli \
    && docker-php-ext-enable mysqli \
    && docker-php-ext-install json \
    && docker-php-ext-enable json \
    && docker-php-ext-install ldap \
    && docker-php-ext-enable ldap \
    && docker-php-ext-install pcntl \
    && docker-php-ext-enable pcntl \
    && rm -rf /root/.cache \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    ;

COPY . /ska

RUN cp /ska/cron /etc/crontab && chmod 600 /etc/crontab \
    && chown -R 166:33 /ska

EXPOSE 9000
ENTRYPOINT ["/ska/entrypoint.sh"]
CMD ["php-fpm"]
